package com.zuitt.batch193;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

public class ArraysSample {

    public static void main(String[] args){
        // Arrays = list of things. In Javascript, we can have as many as we want, but in Java, it is LIMITED.

        // Array Declaration/Instantiation
        int[] intArray = new int[3];
        // int[] - indicates that this is an int data type and  can hold multiple int values
        // new - tells Java to create the said identifier
        // int[3] - indicates the amount of integer values in the array

        intArray[0] = 10;
        intArray[1] = 20;
        intArray[2] = 30;
        // intArray[3] = 40; Index 3 out of bounds for length 3

        System.out.println(intArray[2]);

        // other way to declare an Array
        int intSample[] = new int[3];
        intSample[0] = 50;
        System.out.println(intSample[0]);

        // String Array
        String stringArray[] = new String[3];
        stringArray[0] = "Anya";
        stringArray[1] = "Loid";
        stringArray[2] = "Yor";
        System.out.println(stringArray[0]);

        // Declaration with initialization based on number of elements
        int[] intArray2 = {100, 200, 300, 400, 500};
        System.out.println(intArray2[0]);

        System.out.println(intArray2); // prints out the memory allocation of the array

        // to get the actual value of array
        System.out.println(Arrays.toString(intArray2));
        // Arrays is a class contains various methods for manipulating arrays
        // .toString is a method that returns a string representation of the contents of the specified array.

        // METHODS used in Arrays
        Arrays.sort(stringArray);
        System.out.println(Arrays.toString(stringArray));

        // binarySearch
        String searchTerm = "Anya";
        int binaryResult = Arrays.binarySearch(stringArray, searchTerm);
        System.out.println(binaryResult);
        // binarySearch will return an index if the match is found
        // If the searchTerm is not available in the array, the output will give a result of -(insertion point). The insertion point is defined as the point at which the key would be inserted into the array
        // The array must be sorted as byt the Arrays.sort() method before using the binarySearch() method

        // Multidimensional Arrays
            // two-dimensional array, can be described by two lengths nested each other, like a matrix

        String[][] classroom = new String[3][3];

        // First Row
        classroom[0][0] = "Dahyun";
        classroom[0][1] = "Chaeyoung";
        classroom[0][2] = "Naeyon";
        // Second Row
        classroom[1][0] = "Luffy";
        classroom[1][1] = "Zorro";
        classroom[1][2] = "Sanji";
        // Third Row
        classroom[2][0] = "Loid";
        classroom[2][1] = "Yor";
        classroom[2][2] = "Anya";

        System.out.println(Arrays.deepToString(classroom));
        // deepToString() Returns a string of the nested/deep contents of the array.

        // ArrayLists
        // is a resizable-array. Implements all optional list operations, and permits all elements and can be added or removed whenever it is needed.
        ArrayList<String> students = new ArrayList<>();

        // adding elements to an ArrayList is done by using the add()
        students.add("John");
        students.add("Paul");
        System.out.println(students);

        //  retrieving elements using get()
        System.out.println(students.get(0));

        // update/changing elements using set()
        students.set(1, "George"); // first value -> index second value +> updateValue
        System.out.println(students);

        // delete/remove elements
        students.remove(1); // index number
        System.out.println(students);

        // removing ALL elements in an ArrayList
        students.clear(); // clear all the list of elements
        System.out.println(students);

        // get length of an ArrayList
        System.out.println(students.size());

        // ArrayList with initial values
        ArrayList<String> employees = new ArrayList<>(Arrays.asList("June", "Albert"));
        System.out.println(employees);

        // HashMaps
        // are stored by specifying the data type of the key and of the value
        // flexible object without pattern
        // HashMap<fieldDataType, valueDataType>
        HashMap<String, String> employeeRole = new HashMap<>();

        // adding elements to HashMaps by using put()
        employeeRole.put("Captain", "Luffy");
        employeeRole.put("Doctor", "Chopper");
        employeeRole.put("Navigator", "Name");

        System.out.println(employeeRole);

        // retrieve the value using the field
        System.out.println(employeeRole.get("Captain"));

        // remove fields
        employeeRole.remove("Doctor");
        System.out.println(employeeRole);

        // getting the keys of the elements of HashMap by using the keySet()
        System.out.println(employeeRole.keySet());

        // Hashmaps with int as values
        HashMap<String, Integer> grades = new HashMap<>();

        grades.put("Joe", 89);
        grades.put("John", 93);
        System.out.println(grades);

        // Hashmap with ArrayList
        HashMap<String, ArrayList<Integer>> subjectGrades = new HashMap<>();

        ArrayList<Integer> gradesListA = new ArrayList<>(Arrays.asList(80, 75, 90));
        ArrayList<Integer> gradesListB = new ArrayList<>(Arrays.asList(86, 87, 96));

        subjectGrades.put("Joe", gradesListA);
        subjectGrades.put("John", gradesListB);

        System.out.println(subjectGrades);

        // get a specific value
        System.out.println(subjectGrades.get("Joe").get(2));

    }

}
